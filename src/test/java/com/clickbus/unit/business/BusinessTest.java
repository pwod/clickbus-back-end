package com.clickbus.unit.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import com.clickbus.business.BusinessPlace;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.exception.PlaceNotFoundException;
import com.clickbus.repository.RepositoryPlace;

@SpringBootTest
@DirtiesContext
public class BusinessTest {

	@Autowired
	private BusinessPlace place;

	@Autowired
	private RepositoryPlace repository;

	@BeforeEach
	public void setUp() {
		repository.deleteAll();
	}

	@Test
	public void when_callSavePlace_expected_returnedId() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		Place obj = place.savePlace(objPlace);

		assertEquals("test", obj.getName());
	}

	@Test
	public void when_callListAll_expected_placeNotFound() {
		Assertions.assertThrows(PlaceNotFoundException.class, () -> {
			place.listAll();

		});
	}

	@Test
	public void when_callListAll_expected_list() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		place.savePlace(objPlace);

		List<Place> list = place.listAll();

		assertEquals("test", list.get(0).getName());
	}

	@Test
	public void when_callGetByIdPlace_expected_Object() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		Place obj = place.savePlace(objPlace);

		Place objReturned = place.getByIdPlace(obj.getId());

		assertEquals(obj.getId(), objReturned.getId());
	}

	@Test
	public void when_callGetByIdPlace_expected_Null() {
		Assertions.assertThrows(PlaceNotFoundException.class, () -> {
			place.getByIdPlace(1);

		});
	}

	@Test
	public void when_callGetByNamePlace_expected_placeNotFoundException() {
		Assertions.assertThrows(PlaceNotFoundException.class, () -> {
			place.getByNamePlace("test");
		});
	}

	@Test
	public void when_callGetByNamePlace_expected_sucess() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		Place obj = place.savePlace(objPlace);

		Place objReturned = place.getByNamePlace(obj.getName());

		assertEquals(obj.getName(), objReturned.getName());
	}

	@Test
	public void when_callUpdatePlace_expected_placeNotFoundException() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		Assertions.assertThrows(PlaceNotFoundException.class, () -> {
			place.updatePlace(1, objPlace);
		});
	}

	@Test
	public void when_callUpdatePlace_expected_sucess() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("São Paulo");
		objPlace.setName("test");
		objPlace.setSlug("test");
		objPlace.setState("São Paulo");

		Place obj = place.savePlace(objPlace);

		objPlace.setName("xxxx");
		Place objReturned = place.updatePlace(obj.getId(), objPlace);

		assertEquals("xxxx", objReturned.getName());
	}

}
