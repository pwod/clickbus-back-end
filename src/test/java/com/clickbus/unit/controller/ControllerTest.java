package com.clickbus.unit.controller;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.exception.PlaceNotFoundException;
import com.clickbus.facade.FacadePlace;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;

@SpringBootTest
@DirtiesContext
public class ControllerTest {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@MockBean
	private FacadePlace facadePlace;
	
	@Autowired
	private ObjectMapper mapper;
	
	private static final String URL_ROOT = "/place";
	
	@BeforeEach
	public void setUp() {		
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														
	}
	
	@Test
	@SneakyThrows
	public void when_sendJsonComParametersUnexpected_ReturnBadRequest() { 
		PlaceDto objPlaceDto = new PlaceDto();
		objPlaceDto.setName("test");
		mockMvc.perform(MockMvcRequestBuilders.post(URL_ROOT).content
				(mapper.writeValueAsString(objPlaceDto))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());		

	}
	
	
	@Test
	@SneakyThrows
	public void when_fillingAllParameters_expected_returnsOk() {
		PlaceDto objPlaceDto = new PlaceDto();
		objPlaceDto.setName("test");
		objPlaceDto.setCity("test");
		objPlaceDto.setSlug("test");
		objPlaceDto.setState("test");
		
		Mockito.when(facadePlace.createPlace(objPlaceDto)).thenReturn(new Place());
		
		mockMvc.perform(MockMvcRequestBuilders.post(URL_ROOT).content
				(mapper.writeValueAsString(objPlaceDto))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void when_SendingAn_expected_GetReturnsOkWithList() {
		Mockito.when(facadePlace.listAll()).thenReturn(new ArrayList<Place>());
		
		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());		

	}
	
	
	@Test
	@SneakyThrows
	public void when_sendingGetUrl_expected_returnedNotFound() {
		Mockito.doThrow(PlaceNotFoundException.class).when(facadePlace).listAll();
		
		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void when_getIdPlace_expected_returnedOk() {
				
		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT + "/1" )
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	@SneakyThrows
	public void when_getIdPlace_expected_notFoundPlace() {
		Integer idPlace = 1;
		Mockito.doThrow(PlaceNotFoundException.class).when(facadePlace).getByIdPlace(idPlace);
		
		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT+"/" + idPlace)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void when_getNamePlaceEqualsTest_expected_returnedOk() {
		Mockito.when(facadePlace.findByNamePlace("teste")).thenReturn(new Place());
		
		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT+"/name?name=teste")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void when_getNamePlaceEqualsTest_expected_returendIsNotFound() {
		Mockito.doThrow(PlaceNotFoundException.class).when(facadePlace).findByNamePlace("teste");

		mockMvc.perform(MockMvcRequestBuilders.get(URL_ROOT+"/name?name=teste")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void 
	when_fillingInAllParameters_expected_returnsNotFound() {
		PlaceDto objPlaceDto = new PlaceDto();
		objPlaceDto.setName("test");
		objPlaceDto.setCity("test");
		objPlaceDto.setSlug("test");
		objPlaceDto.setState("test");
		
		Mockito.doThrow(PlaceNotFoundException.class).when(facadePlace).updatePlace(1, objPlaceDto);
		
		mockMvc.perform(MockMvcRequestBuilders.put(URL_ROOT + "/1").content
				(mapper.writeValueAsString(objPlaceDto))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());		
		
	}
	
	
	@Test
	@SneakyThrows
	public void when_fillingInAllParameters_expected_returnsOk() {
		PlaceDto objPlaceDto = new PlaceDto();
		objPlaceDto.setName("test");
		objPlaceDto.setCity("test");
		objPlaceDto.setSlug("test");
		objPlaceDto.setState("test");
		
		Mockito.when(facadePlace.updatePlace(1, objPlaceDto)).thenReturn(new Place());
		
		mockMvc.perform(MockMvcRequestBuilders.put(URL_ROOT + "/1").content
				(mapper.writeValueAsString(objPlaceDto))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());		
		
	}
	

}
