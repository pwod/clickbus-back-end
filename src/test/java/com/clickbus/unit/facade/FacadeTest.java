package com.clickbus.unit.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import com.clickbus.business.BusinessPlace;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.facade.FacadePlace;

@SpringBootTest
@DirtiesContext
public class FacadeTest {
	
@Autowired
private FacadePlace place;

@MockBean
private BusinessPlace businessPlace;

     @Test
     public void when_callMethodSaveSaveAllParameters_Expected_ReturnSuccess() {
    	 PlaceDto objPlace = new PlaceDto();
    	 objPlace.setCity("test");
    	 objPlace.setName("test");
    	 objPlace.setSlug("test");
    	 objPlace.setState("test");
    	 
    	 Mockito.when(businessPlace.savePlace(objPlace)).thenReturn(objPlace.convertToPlaceEntityDomain(objPlace));
    	 
    	Place objReturned = place.createPlace(objPlace);
    	 
    	 assertEquals("test", objReturned.getCity());
     }
     
     
     @Test
     public void when_getListAllPlaces_expected_listSizePlusOne() {
    	 Place objPlace = new Place();
    	 
    	 objPlace.setId(1);
    	 objPlace.setUpdatedAt(LocalDateTime.now());
    	 objPlace.setCreatedAt(LocalDateTime.now());
    	 objPlace.setCity("test");
    	 objPlace.setName("test");
    	 objPlace.setSlug("test");
    	 objPlace.setState("test");
    	 
    	 List<Place> listPlace = new ArrayList<>();
    	 listPlace.add(objPlace);
    	 
    	 Mockito.when(businessPlace.listAll()).thenReturn(listPlace);
    	 
    	List<Place> list = place.listAll();
    	 
    	 assertTrue(list.size() > 0);
     }
     
     
     @Test
     public void when_callByIdPlace_expected_returnedObject() {
    	 PlaceDto objPlace = new PlaceDto();
    	 
    	 objPlace.setCity("test");
    	 objPlace.setName("test");
    	 objPlace.setSlug("test");
    	 objPlace.setState("test");
    	 
    	 Mockito.when(businessPlace.getByIdPlace(1)).thenReturn(objPlace.convertToPlaceEntityDomain(objPlace));
    	 
    	Place objReturned = place.getByIdPlace(1);
    	 
    	 assertEquals("test", objReturned.getCity());
     }
     
     @Test
     public void when_callByIdPlace_expected_returnedEmpty() {
    	 Mockito.when(businessPlace.getByIdPlace(1)).thenReturn(new Place());
    	 Place objReturned = place.getByIdPlace(1);
    	 assertEquals(null, objReturned.getCity());
     }
     
     
     @Test
     public void when_callByNamePlace_expected_returnedObject() {
    	 PlaceDto objPlace = new PlaceDto();
    	 
    	 objPlace.setCity("test");
    	 objPlace.setName("test");
    	 objPlace.setSlug("test");
    	 objPlace.setState("test");
    	 
    	 Mockito.when(businessPlace.getByNamePlace("test")).thenReturn(objPlace.convertToPlaceEntityDomain(objPlace));
    	 
    	Place objReturned = place.findByNamePlace("test");
    	 
    	 assertEquals("test", objReturned.getCity());
     }
     
     @Test
     public void when_callByNamePlace_expected_returnedNull() {

    	 Mockito.when(businessPlace.getByNamePlace("test")).thenReturn(new Place());
    	 
    	Place objReturned = place.findByNamePlace("test");
    	 
    	 assertEquals(null, objReturned.getCity());
     }
     
     
	@Test
	public void when_callUpdatePlace_expected_successUpdate() {
		PlaceDto objPlace = new PlaceDto();

		objPlace.setCity("test");
		objPlace.setName("xxxx");
		objPlace.setSlug("test");
		objPlace.setState("test");

		Mockito.when(businessPlace.updatePlace(1, objPlace)).thenReturn(objPlace.convertToPlaceEntityDomain(objPlace));

		Place objReturned = place.updatePlace(1, objPlace);

		assertEquals("xxxx", objReturned.getName());
	}
     

}
