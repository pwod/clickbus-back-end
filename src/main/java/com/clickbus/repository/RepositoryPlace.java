package com.clickbus.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.clickbus.domain.Place;

public interface RepositoryPlace extends JpaRepository<Place, Integer> {
	
	public Optional<Place> findByName(String name);

}
