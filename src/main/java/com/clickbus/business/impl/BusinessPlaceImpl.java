package com.clickbus.business.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clickbus.business.BusinessPlace;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.exception.PlaceNotFoundException;
import com.clickbus.repository.RepositoryPlace;

import lombok.SneakyThrows;

@Service
public class BusinessPlaceImpl implements BusinessPlace {
	
	@Autowired
	private RepositoryPlace repository;
	
	@Override
	public Place savePlace(PlaceDto place) {
		return repository.save(place.convertToPlaceEntityDomain(place));
	}
	
	
	@Override
	public List<Place> listAll() {
		List<Place> listPlace = repository.findAll();
		
		if(listPlace.isEmpty()) {
			throw new PlaceNotFoundException("not found Places");
		}
		
		return listPlace;
	}
	
	@Override
	@SneakyThrows
	public Place getByIdPlace(Integer idPlace) {
		Optional<Place> objPlace = repository.findById(idPlace);

		if (objPlace.isPresent()) {
			return objPlace.get();
		} else {
			throw new PlaceNotFoundException("No records found by id Place.");
		}

	}
	
	@Override
	@SneakyThrows
	public Place getByNamePlace(String namePlace) {
		Optional<Place> objPlace = repository.findByName(namePlace);

		if (objPlace.isPresent()) {
			return objPlace.get();
		} else {
			throw new PlaceNotFoundException("not found Place by name.");
		}

	}
	
	@Override
	public Place updatePlace(Integer id, PlaceDto place) {
		Optional<Place> obj = repository.findById(id);

		if (obj.isPresent()) {
			Place objPlace = obj.get();
			
			objPlace.setCity(place.getCity());
			objPlace.setName(place.getName());
			objPlace.setSlug(place.getSlug());
			objPlace.setState(place.getState());
			objPlace.setUpdatedAt(LocalDateTime.now());
			
			return repository.save(objPlace);

		} else {
			throw new PlaceNotFoundException("not found Place for Updated.");
		}
		
	}


}
