package com.clickbus.business;

import java.util.List;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;

public interface BusinessPlace {

	Place savePlace(PlaceDto place);

	List<Place> listAll();

	Place getByIdPlace(Integer idPlace);

	Place getByNamePlace(String namePlace);

	Place updatePlace(Integer id, PlaceDto place);


}
