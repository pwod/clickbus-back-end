package com.clickbus.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.facade.FacadePlace;

@RestController
@RequestMapping("place")
public class ControllerPlace {
	
	@Autowired
	private FacadePlace facadePlace;
	

	@PostMapping
	public ResponseEntity<Place> createPlace(@Valid @RequestBody PlaceDto place) {
		Place obj = facadePlace.createPlace(place);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().
				path("/{id}").buildAndExpand(obj.getId()).toUri();
		
		return ResponseEntity.created(uri).body(obj);
	}
	
	@GetMapping
	public List<Place> listPlaces() {
		return facadePlace.listAll();
			
	}
	
	@GetMapping(value = "/{id}")
	public Place getByIdPlace(@PathVariable Integer id) {
		return facadePlace.getByIdPlace(id);
		
	}
	
	@GetMapping(value = "/name")
	public Place searchByNamePlace(@RequestParam String name) {
		return facadePlace.findByNamePlace(name);
			
	}
	
	@PutMapping(value = "/{id}")
	public Place updatePlace (@PathVariable Integer id, @RequestBody PlaceDto place ) {
		return facadePlace.updatePlace(id, place);
	}

}
