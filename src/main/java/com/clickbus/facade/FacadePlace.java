package com.clickbus.facade;

import java.util.List;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;

public interface FacadePlace {

	Place createPlace(PlaceDto place);

	List<Place> listAll();

	Place getByIdPlace(Integer idPlace);

	Place findByNamePlace(String name);

	Place updatePlace(Integer id, PlaceDto place);

}
