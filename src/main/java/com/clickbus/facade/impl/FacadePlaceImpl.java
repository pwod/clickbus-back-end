package com.clickbus.facade.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clickbus.business.BusinessPlace;
import com.clickbus.domain.Place;
import com.clickbus.dto.PlaceDto;
import com.clickbus.facade.FacadePlace;

@Service
public class FacadePlaceImpl implements FacadePlace {
	
	@Autowired
	private BusinessPlace businessPlace;

	@Override
	public  Place createPlace(PlaceDto place) {
		return businessPlace.savePlace(place);
	}
	
	@Override
	public  List<Place> listAll() {
		return businessPlace.listAll();
	}
	
	@Override
	public Place getByIdPlace(Integer idPlace) {
		return businessPlace.getByIdPlace(idPlace);
	}

	@Override
	public Place findByNamePlace(String name) {
		return businessPlace.getByNamePlace(name);
	}
	
	@Override
	public Place updatePlace(Integer id, PlaceDto place) {
		return businessPlace.updatePlace(id, place);
	}
}
