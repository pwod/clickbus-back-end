package com.clickbus.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import com.clickbus.domain.Place;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlaceDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "input name required")
	private String name;

	@NotBlank(message = "input slug required")
	private String slug;
	
	@NotBlank(message = "input city required")
	private String city;
	
	@NotBlank(message = "input state required")
	private String state;
	
	/**
	 * 
	 * @param the obj PlaceDto.
	 * @return
	 */
	public Place convertToPlaceEntityDomain(PlaceDto obj) {
		Place place = new Place();
		place.setCity(obj.getCity());
		place.setName(obj.getName());
		place.setSlug(obj.getSlug());
		place.setState(obj.getState());
		place.setCreatedAt(LocalDateTime.now());
		place.setUpdatedAt(LocalDateTime.now());
		
		return place;
	}
	
	/**
	 * 
	 * @param the id.
	 * @param the obj PlaceDto.
	 * @return
	 */
	public Place convertToPlaceEntityDomain(Integer id, PlaceDto obj) {
		Place place = new Place();
		place.setId(id);
		place.setCity(obj.getCity());
		place.setName(obj.getName());
		place.setSlug(obj.getSlug());
		place.setState(obj.getState());
		place.setUpdatedAt(LocalDateTime.now());
		
		return place;
	}

}
