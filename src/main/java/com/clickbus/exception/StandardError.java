package com.clickbus.exception;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class StandardError Used to format general error messages.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StandardError implements Serializable {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The apicode. */
    @JsonProperty("message")
    private String message;
}
