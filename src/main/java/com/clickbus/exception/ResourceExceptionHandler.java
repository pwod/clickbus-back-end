package com.clickbus.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(PlaceNotFoundException.class)
	public ResponseEntity<StandardError> handlePlaceNotFoundException(PlaceNotFoundException ex,
			HttpServletRequest request) {

		StandardError error = new StandardError(ex.getMessage());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}

}
