Teste finalizado.

Candidato: Patrick Wellington Duarte de Oliveira.

Publicado aplicação no Heroku. URL: https://click-bus-backend-developer.herokuapp.com/place

End Points:

POST -> https://click-bus-backend-developer.herokuapp.com/place
parametros em Json (exemplo):

{
	"name": "teste",
	"slug": "Ola",
	"city" : "Itapecerica da Serra",
	"state" : "São Paulo"
}

Get -> https://click-bus-backend-developer.herokuapp.com/place   (retorna todos os registros no Banco).

Get -> https://click-bus-backend-developer.herokuapp.com/place/1  <-(id para retornar só um Place).

Get por nome -> https://click-bus-backend-developer.herokuapp.com/place/name?name=teste

PUT -> https://click-bus-backend-developer.herokuapp.com/place

parametros em Json (exemplo):

{
	"name": "teste",
	"slug": "Ola",
	"city" : "Itapecerica da Serra",
	"state" : "São Paulo"
}

